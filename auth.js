const jwt = require('jsonwebtoken')

const secret = 'CourseBookingAPI'

// token creation
module.exports.createAccessToken=(user)=>{

	// when the user login, the token will be created with the user's information
	const data ={
		id:user._id,
		email:user.email,
		isAdmin:user.isAdmin
	}

	// Generate a json web token using the jwt's sign method
	// generates the token using the form data and the secret code with no additional options
	return jwt.sign(data, secret, {})
}

module.exports.verify=(req,res,next)=>{
	let token=req.headers.authorization

	if(typeof token !== "undefined"){
		console.log(token)
		token = token.slice(7,token.length)
		return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return res.send({auth:'failed'})
			} else{
				next()
			}
		})
	} else{
		return res.send({auth:'failed'})
	}
}

module.exports.decode= (token)=>{
	if(typeof token !== "undefined"){
		console.log(token)
		token = token.slice(7,token.length)
		return jwt.verify(token,secret,(err,data)=>{
			if(err){
				return null
			} else{
				return jwt.decode(token,{complete:true}).payload
			}
		})
	} else{
		return null
	}
}