const express = require('express')
const port = 4000
const mongoose=require('mongoose')
// allows our backend application to be available to our frontend application
// allow us to control the app's Cross Origin Resource Sharing settings 
const cors=require('cors')
const app = express()
const userRoutes=require('./routes/userRoutes')
const courseRoutes=require('./routes/courseRoutes')


// allows all resources to access our backend application
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use('/users',userRoutes)
app.use('/courses',courseRoutes)



mongoose.connect('mongodb+srv://admin:admin123@course-booking.flw5f.mongodb.net/s32-s36?retryWrites=true&w=majority',
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
})

let db = mongoose.connection
// if connection error occured, output error message in console
db.on('error', console.error.bind(console,'Connection Error'))
// if connection is successful, output message in console
db.once('open',()=>console.log("Now connected to MongoDB Atlas"))

// will use the defined port number for the application whenever an environment variable is available or will use port 4000 if none is defined
// this syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || port, ()=>{
	console.log(`API is now online on port ${process.env.PORT || port}`)
})