const Course=require("../models/Course")
const auth = require('../auth')

module.exports.addCourse=(reqBody)=>{
	
	// if (data.admin==true){
	let newCourse=new Course({
		name:reqBody.name,
		description:reqBody.description,
		price:reqBody.price
	})

	
	return newCourse.save().then((course,error)=>{
		if(error){
			return false
		} else {
			return true
		}
	})
} 
// else {
	// return false

// }

// retrieve all courses
module.exports.getAllCourses=()=>{
	return Course.find({}).then(result=>result)
}

// retrieve all active courses
module.exports.getAllActive=()=>{
	return Course.find({isActive:true}).then(result=>result)
}

// retrieve specific course
module.exports.getCourse=(reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>result)
}

// update course
module.exports.updateCourse=(data)=>{
	console.log(data)
	return Course.findById(data.courseId).then((result,error)=>{
		console.log(result)
		if (data.isAdmin){
			result.name=data.updatedCourse.name
			result.description=data.updatedCourse.description
			result.price=data.updatedCourse.price

			console.log(result)

			return result.save().then((updatedCourse,error)=>{
				if (error){
					return false
				} else{
					return updatedCourse
				}
			})
		} else{
			return 'Not Admin'
		}
	})
}

// archive course
module.exports.archiveCourse=(data)=>{
	return Course.findById(data.courseId).then((result)=>{
		// console.log(result)
		if(data.isAdmin){
			result.isActive=false
			// console.log(result)
			return result.save().then((archivedCourse,error)=>{
				if (error){
					return false
				} else{
					return true
				}
			})
			
		} else{
			return 'Not Admin'
		}
	})

}