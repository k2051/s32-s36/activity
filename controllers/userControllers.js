const User = require('../models/User')
const Course = require('../models/Course')
const bcrypt=require('bcrypt')
const auth = require('../auth')

// Check if email exists
module.exports.checkEmailExists=(reqBody)=>{
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			return true
		} else{
			return false
		}
	})
}

// register user
module.exports.registerUser=(reqBody)=>{
	let newUser= new User({
		firstName:reqBody.firstName,
		lastName:reqBody.lastName,
		email:reqBody.email,
		mobileNo:reqBody.mobileNo,
		// 10 is the value provided as the number of 'salt' rounds that the bcrpyt algorithm will run in order to encrypt the password/data
		// hashSync(<dataToBeHashed>,<salt)
		password:bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error)=>{
		if(error){
			return false
		} else {
			return true
		}
	})
}

// user authentication
module.exports.loginUser=(reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false
		} else{
			// compareSync(dataToBeCompared,encryptedData)
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password)
			if (isPasswordCorrect) {
				return{access:auth.createAccessToken(result)}
			} else{
				return false
			}

		}
	})
}

// retrieve user
module.exports.getProfile=(data)=>{
	return User.findById(data.userId).then(result=>{
		if(result==null){
			return false
		} else{
			result.password=''
			return result
		}
	})
}

// enroll user to course
module.exports.enroll = async (data)=>{
	console.log(data)
	let isUserUpdated= await User.findById(data.userId).then(user=>{
		user.enrollments.push({courseId:data.courseId})
		return user.save().then((user,error)=>{
			if (error){
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		course.enrollees.push({userId:data.userId})
		return course.save().then((user,error)=>{
			if (error) {
				return false
			} else {
				return true
			}
		})
	})

	if(isUserUpdated && isCourseUpdated){
		return true
	} else{
		return false
	}
}