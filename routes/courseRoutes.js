const express = require('express')
const router = express.Router()
const courseController=require('../controllers/courseControllers')
const auth = require('../auth')

// route for creating course
router.post('/',auth.verify,(req,res)=>{
	const userData=auth.decode(req.headers.authorization)
	console.log(userData)
	if(userData.isAdmin==true){
		courseController.addCourse(req.body).then(resultFromController=>res.send(resultFromController))
	} else {
		res.send('User is not an admin')
	}
	
})

// route for getting all courses
router.get('/all',auth.verify,(req,res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController))
}) 

// route for retrieving all courses
router.get('/',(req,res)=>{
	courseController.getAllActive().then(resultFromController=>res.send(resultFromController))
})

// route for retrieving specific course
router.get('/:courseId', (req,res)=>{
	console.log(req.params.courseId)
	courseController.getCourse(req.params).then(resultFromController=>res.send(resultFromController))
})

// route for updating course
router.put('/:courseId',auth.verify,(req,res)=>{
	const data={
		courseId:req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse:req.body
	}
	courseController.updateCourse(data).then(resultFromController=>res.send(resultFromController))
})

// route for archiving course
router.put('/:courseId/archive',auth.verify,(req,res)=>{
	const data={
		courseId:req.params.courseId,
		isAdmin:auth.decode(req.headers.authorization).isAdmin
	}
	// console.log(data)
	courseController.archiveCourse(data).then(resultFromController=>res.send(resultFromController))
})
module.exports=router