const express = require('express')
const router = express.Router()
const userController=require('../controllers/userControllers')
const auth = require('../auth')
// route for checking email exists
router.post('/checkEmail',(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

// route for user registration
router.post('/register',(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

// route for user authentication
router.post('/login',(req,res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController))
})

// route for user retrieve
// the auth.verify will act as a middleware to ensure that the user is logged in before they can get the details
router.get('/details',auth.verify,(req,res)=>{

	const userData=auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({userId:userData.id}).then(resultFromController=>{
		if(resultFromController==false){
			res.send("user does not exist")
		} else{
		res.send(resultFromController)
		}
	})
})

router.post('/enroll',auth.verify,(req,res)=>{
	let data ={
		userId:auth.decode(req.headers.authorization).id,
		courseId:req.body.courseId
	}
	// console.log(data)
	let admin=auth.decode(req.headers.authorization).isAdmin
	if (admin==false){
		userController.enroll(data).then(resultFromController=>res.send(resultFromController))
	} else {
		return res.send('User is an admin')
	}

})

module.exports=router